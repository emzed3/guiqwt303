guiqwt303
=========

This repository builds and uploads wheels for `guiqwt` version `3.0.3` patched to work with Python `3.9`, `3.10` and `3.11`.

The original code is hosted at https://github.com/PierreRaybaut/guiqwt

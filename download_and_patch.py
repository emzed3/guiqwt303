#!/usr/bin/env python

import glob
import os
import shutil
import sys
import tempfile
import zipfile
from io import BytesIO

import requests
from setuptools.build_meta import *

HERE = os.path.dirname(os.path.abspath(__file__))


def download_and_unpack(target, version_modifier):
    # https://warehouse.pypa.io/api-reference/integration-guide.html#predictable-urls
    url = "https://files.pythonhosted.org/packages/source/g/guiqwt/guiqwt-3.0.3.zip"
    if target is None:
        target = tempfile.mkdtemp()
    r = requests.get(url, allow_redirects=True, stream=True)
    z = zipfile.ZipFile(BytesIO(r.content))
    z.extractall(target)
    return os.path.join(target, "guiqwt-3.0.3")


def patch_guiqwt(folder):
    with open(os.path.join(folder, "pyproject.toml"), "w") as fh:
        print("[build-system]", file=fh)
        print(
            'requires = ["setuptools", "cython", "wheel", "numpy", "guidata==1.7.7",'
            '"PyQt5", "PyQt5-Qt5", "pyqt5-sip"]',
            file=fh,
        )

    shutil.move(f"{folder}/guiqwt", f"{folder}/guiqwt303")
    with open(os.path.join(folder, "setup.py")) as fh:
        before = fh.readlines()

    with open(os.path.join(folder, "setup.py"), "w") as fh:
        print(
            """
import PyQt5.sip
import sys
sys.modules["sip"] = PyQt5.sip
import collections
import collections.abc
collections.MutableMapping = collections.abc.MutableMapping
import gettext

orig = gettext.translation

def _translation(*a, **kw):
    kw.pop("codeset", None)
    return orig(*a, **kw)

gettext.translation = _translation

from setuptools import Extension as _Extension, setup

def Extension(*a, **kw):
    kw["include_dirs"] = kw.get("include_dirs", []) + [numpy.get_include()]
    return _Extension(*a, **kw)


""",
            file=fh,
        )
        for line in before:
            if line.startswith("from __future"):
                continue
            if line.startswith("from numpy.distutils"):
                continue
            line = line.replace("PythonQwt>=0.5.0", "PythonQwt==0.5.5")
            line = line.replace("guidata>=1.7.0", "guidata==1.7.7")
            if not "guiqwt:" in line:  # skips head line in docs
                line = line.replace("guiqwt", "guiqwt303")
            line = line.replace(
                "version=__version__", f"version=__version__ + '{version_modifier}'"
            )
            print(line.rstrip(), file=fh)


if __name__ == "__main__":
    import sys

    folder, *rest = sys.argv[1:]
    version_modifier = rest[0] if len(rest) else ""

    guiqwt_folder = download_and_unpack(folder, version_modifier)
    patch_guiqwt(guiqwt_folder)
